package gofpdf

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"runtime"
	"strings"

	"github.com/mandykoh/prism"
	"github.com/mandykoh/prism/adobergb"
	"github.com/mandykoh/prism/ciexyz"
	"github.com/mandykoh/prism/displayp3"
	"github.com/mandykoh/prism/prophotorgb"
	"github.com/mandykoh/prism/srgb"
)

var ErrUnknownColorProfile error = errors.New("unknown color profile")

func convertToSRGB(metaData ImageMetaData, r io.Reader) (*bytes.Buffer, error) {
	if metaData.ColorProfile == "" {
		return nil, nil
	}

	img, t, err := image.Decode(r)
	if err != nil {
		return nil, fmt.Errorf("failed to decode image %w", err)
	}

	src := prism.ConvertImageToNRGBA(img, runtime.NumCPU())
	dst := image.NewNRGBA(src.Rect)
	// whitePoint := srgb.StandardWhitePoint
	// var colorFromNRGBA func(color.NRGBA) (Color, float32)

	colorProfile := strings.ToLower(metaData.ColorProfile)
	if strings.Contains(colorProfile, "pro") {
		adaptation := ciexyz.AdaptBetweenXYYWhitePoints(
			prophotorgb.StandardWhitePoint,
			srgb.StandardWhitePoint,
		)

		for i := src.Rect.Min.Y; i < src.Rect.Max.Y; i++ {
			for j := src.Rect.Min.X; j < src.Rect.Max.X; j++ {
				inCol, alpha := prophotorgb.ColorFromNRGBA(src.NRGBAAt(j, i))
				xyz := inCol.ToXYZ()
				xyz = adaptation.Apply(xyz)
				outCol := srgb.ColorFromXYZ(xyz)
				dst.SetNRGBA(j, i, outCol.ToNRGBA(alpha))
			}
		}

		return imageEncode(t, dst)
	} else if strings.Contains(colorProfile, "adobe") {
		adaptation := ciexyz.AdaptBetweenXYYWhitePoints(
			adobergb.StandardWhitePoint,
			srgb.StandardWhitePoint,
		)

		for i := src.Rect.Min.Y; i < src.Rect.Max.Y; i++ {
			for j := src.Rect.Min.X; j < src.Rect.Max.X; j++ {
				inCol, alpha := adobergb.ColorFromNRGBA(src.NRGBAAt(j, i))
				xyz := inCol.ToXYZ()
				xyz = adaptation.Apply(xyz)
				outCol := srgb.ColorFromXYZ(xyz)
				dst.SetNRGBA(j, i, outCol.ToNRGBA(alpha))
			}
		}

		return imageEncode(t, dst)
	} else if strings.Contains(colorProfile, "display") {

		adaptation := ciexyz.AdaptBetweenXYYWhitePoints(
			displayp3.StandardWhitePoint,
			srgb.StandardWhitePoint,
		)

		for i := src.Rect.Min.Y; i < src.Rect.Max.Y; i++ {
			for j := src.Rect.Min.X; j < src.Rect.Max.X; j++ {
				inCol, alpha := displayp3.ColorFromNRGBA(src.NRGBAAt(j, i))
				xyz := inCol.ToXYZ()
				xyz = adaptation.Apply(xyz)
				outCol := srgb.ColorFromXYZ(xyz)
				dst.SetNRGBA(j, i, outCol.ToNRGBA(alpha))
			}
		}

		return imageEncode(t, dst)
	} else if strings.Contains(colorProfile, "srgb") {
		return nil, nil
	}

	return nil, fmt.Errorf("%w [color_profile=%s]", ErrUnknownColorProfile, metaData.ColorProfile)
}

func imageEncode(imgType string, img image.Image) (*bytes.Buffer, error) {
	var err error
	buffer := bytes.NewBuffer([]byte{})

	switch imgType {
	case "jpeg", "jpg":
		err = jpeg.Encode(buffer, img, &jpeg.Options{Quality: 95})
	case "png":
		err = png.Encode(buffer, img)
	case "gif":
		err = gif.Encode(buffer, img, nil)
	}

	return buffer, err
}
