module gitlab.com/smallwoods/gofpdf

go 1.18

require (
	github.com/boombuler/barcode v1.0.1
	github.com/mandykoh/prism v0.35.0
	github.com/phpdave11/gofpdi v1.0.13
	github.com/ruudk/golang-pdf417 v0.0.0-20201230142125-a7e3863a1245
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410
)

require (
	github.com/mandykoh/go-parallel v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
